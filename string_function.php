<?php
//addSlashes function
$str="Hell'o World! How's Life";
echo addslashes($str)."<br><br>";


//implode Function
$my_array = array("Rohim","Korim","Jamal");

echo '"'.implode('","',$my_array).'"'."<br><br>";

//explode Function
$myStr = "Rohim Korim Jamal";

$my_array = explode(" ",$myStr);

echo "<pre>";
    print_r($my_array);
echo "</pre>";

//htmlEntities function

$myTutorial = "<br> Means Line Break";
echo htmlentities($myTutorial)."<br>";


//trim function
$username = " Rohim ";
echo $username."<br>";
echo trim($username)."<br><br>";

//lTrim function
$myString = "Hello world!";
echo $myString."<br>";
echo ltrim($myString,"Hello")."<br><br>";

//rTrim function
$myString = "Hello world!";
echo $myString."<br>";
echo rtrim($myString,"world!")."<br><br>";

//nl2br function
$myStr = "Line1\nLine2\nLine3\n";
echo "<br><br>".nl2br($myStr)."<br><br>";

//str_pad function
$myStr = "Hello World! How's Life";

echo str_pad($myStr,100,"$myStr")."<br><br>";

//str_repeat function
$myStr = "AB";
echo str_repeat($myStr,5)."<br><br>";

//str_replace function
$myStr = "Shantu Chowdhury";
echo str_replace("S","B","C",$myStr)."<br><br>";

//split function
$my_array = str_split($myStr,5);
echo "<pre>";
    print_r($my_array)."<br><br>";
echo "</pre>";

//strLen Function
echo strlen($myStr);

//strToUpper function
echo strtoupper($myStr);

//strToUpper function
echo strtolower($myStr);

//subStrCompare function
$str1 = "Shantu is chowdhury";
echo substr_compare($str1,"Shantu is chowdhury",0)."<br><br>";

//subStr Replace function
$myStr = "Shantu showdhury";
echo substr_replace($myStr,"Chowdhury",7)."<br><br>";

//ucFirst function
$myStr = "my name is ashok barua";
echo ucfirst($myStr)."<br><br>";

//ucWords function
echo ucwords($myStr)."<br><br>";

//lcFirst function
$myStr = "My Name Is Ashok Barua";
echo lcfirst($myStr);