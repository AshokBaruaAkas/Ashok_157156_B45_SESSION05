<?php
    //Boolean Start Here
        $decition = true;
        if($decition){
            echo "The Decition is True!";
        }
        $decition = false;
        if($decition){
            echo "The Decition is False!";
        }
    //Boolean End Here

    echo "<br>";

    //Integer & Float Example Start Here
        $value1 = 100;      //Integer
        $value2 = 55.35;    //float

    //Integer & Float Example End Here

    echo "<br>";

    //String Example Start Here
        $my_string1 = 'abcd1234#$% $value1';

        $my_string2 = "abcd1234#$% $value1";

    echo $my_string1."<br>";
    echo $my_string2."<br>";
    //String Example End here

    echo "<br>";

//HereDoc Start Here
$heredocString =<<<BITM
    heredoc Line1 $value1 <br>
    heredoc Line2 $value1 <br>
    heredoc Line3 $value1 <br>
BITM;
echo $heredocString."<br>";
//HereDoc End Here

    echo "<br>";

//NowDoc Start Here
$nowdocString =<<<'BITM'
nowdoc Line1 $value1 <br>
nowdoc Line2 $value1 <br>
nowdoc Line3 $value1 <br>
BITM;
echo $nowdocString."<br>";
//Nowdoc End Here

    echo "<br>";

    //Array Example Start Here
    //index array
    $indexedArray = array(1,2,3,4,5,6,7);
    print_r($indexedArray);

    echo "<br>";

    $indexedArray = array("Toyota","BMW","Jaguar",1,2.5,3,"Nissan","Ford");
    print_r($indexedArray);

    echo "<br>";

    //Associative array
    $ageArray = array("Rahim"=>34,"MoynarMa"=>54,"Kuddus"=>40,"Abul"=>90);
    echo "<pre>";
    print_r($ageArray);
    echo "</pre>";

    echo "<br>";

    $ageArray['MoynarMa'] = 55;
    print_r($ageArray['MoynarMa']);
//Array Example End Here

?>