<?php
//Test floatval() function.
    $var = '122.2154The';

    $float_Value = floatval($var);

    echo $float_Value;

    echo "<br><br>";

//Test empty() function
    $var = "";
    if(empty($var)){
    echo '$var is Empty';
    }
    else{
        echo '$var is not Empty';
    }

    echo "<br><br>";

//Test is_array() function
    $var = array(12,2,2,32,5);
    if(is_array($var)){
    echo '$var is An array';
    }
    else{
    echo '$var is not An Array';
    }

    echo "<br><br>";

//Test is_null() Function
    $value = null;
    if(is_null($value)){
        echo "This is Null Value.";
    }
    else{
        echo "This is Not Null Value";
    }

    echo "<br><br>";

//Test is_object() function
    $object = new stdClass();
    if(is_object($object)){
        echo "This is a Object.";
    }
    else{
        echo "This is Not a Object.";
    }

    echo "<br><br>";

//Test isset() function
    $value;
    if(isset($value)){
        echo "This Variable is Set.";
    }
    else{
        echo "This Variable is UnSet.";
    }

    echo "<br><br>";

//Test print_r() function
    $age = array('Ashok'=>20,'Sumon'=> array('Sumon'=>19,'Dipa'=>18,),'Shantu'=>18);
    print_r($age);

    echo "<br><br>";

//Test serialize() function
    $data = array(10,23,'a'=>34,'b'=>"Ashok");
    $serializeData = serialize($data);
    echo $serializeData;

    echo "<br><br>";

//Test Unserialize() function
    print_r(unserialize($serializeData));

    echo "<br><br>";

//Test unset() function
    $test = "testText";
    unset($test);
    if(isset($test)){
        echo "This Variable is Seted.";
    }
    else{
        echo "This Variable is UnSeted.";
    }

    echo "<br><br>";

//Test var_dump() function
    $data = array(10,23,'a'=>34,'b'=>"Ashok");
    var_dump($data);

    echo "<br><br>";

//Test var_export() function
    var_export($data);

    echo "<br><br>";

//Test gettype() function
    echo gettype($data);

    echo "<br><br>";

//Test is_bool() function
    $bool_var = true;
    if(is_bool($bool_var)){
        echo '$bool_var is Boolean Variable.';
    }
    else{
        echo '$bool_var is not Boolean Variable.';
    }

    echo "<br><br>";

//Test is_float(),is_int(),is_string() & is_scalar function
    $varString = "This is a String.";
    $varInt = 20;
    $varFloat = 55.55;
    if(is_string($varString)){
        echo $varString."<br><br>";
    }
    if(is_int($varInt)){
        echo '$varInt is Integer Value.<br><br>';
    }
    if(is_float($varFloat)){
        echo '$varFloat is Integer Value.<br><br>';
    }
    if(is_scalar($varFloat)){
        echo 'This is a Scalar.<br><br>';
    }

//Test bool_val() function
    if(boolval(1)){
        echo 'Return True.';
    }

echo "<br><br>";

//Test intVal() Function
    echo intval(042);